CREATE TABLE public."tblCourse"
(
  id bigint NOT NULL DEFAULT nextval('"tblCourse_id_seq"'::regclass),
  "nameCourse" character varying(255) NOT NULL,
  CONSTRAINT pk_course PRIMARY KEY (id),
  CONSTRAINT unq_name UNIQUE ("nameCourse")
)

CREATE TABLE public."tblMatricula"
(
  "idStudent" bigint NOT NULL,
  "idCourse" bigint NOT NULL,
  qualification character varying(10) NOT NULL DEFAULT '0'::character varying,
  qualified boolean NOT NULL DEFAULT false,
  CONSTRAINT "fk_idStudent" FOREIGN KEY ("idStudent")
      REFERENCES public."tblStrudent" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_idcourse FOREIGN KEY ("idCourse")
      REFERENCES public."tblCourse" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unq_matricula UNIQUE ("idStudent", "idCourse")
)

CREATE TABLE public."tblStrudent"
(
  id bigint NOT NULL DEFAULT nextval('"Strudent_id_seq"'::regclass),
  name character varying(255) NOT NULL,
  "lastName" character varying NOT NULL,
  CONSTRAINT pk_student PRIMARY KEY (id),
  CONSTRAINT unq_student UNIQUE (name, "lastName")
)