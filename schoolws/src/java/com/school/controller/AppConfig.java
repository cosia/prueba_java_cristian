package com.school.controller;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Cristian Z. Osia
 */

@ApplicationPath("ws")
public class AppConfig extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet();
        addResources(resources);
        return resources;
    }
    
    public void addResources(Set<Class<?>> resources) {
        
    }
}
