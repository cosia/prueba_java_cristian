package com.school.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class Enrollment {
    
    private int idStudent;
    private int idCourse;
    private double qualification;
    private boolean qualified;

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public double getQualification() {
        return qualification;
    }

    public void setQualification(double qualification) {
        this.qualification = qualification;
    }
    
    public void setQualificationFromString(String qualification) {
        this.qualification = Double.parseDouble(qualification);
    }

    public boolean isQualified() {
        return qualified;
    }

    public void setQualified(boolean qualified) {
        this.qualified = qualified;
    }
    
}
