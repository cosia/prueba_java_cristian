package com.school.rest;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.injection.Injection;
import com.school.interactors.StudentDaoImp;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Cristian Z. Osia
 */

@Path("/student")
public class StudentRest {    
        
    @POST
    @Path("allstudents")
    @Produces("application/json")
    public String getAllStudents() throws Exception {
        StudentDaoImp sd = new StudentDaoImp();
        return Injection.provideGson().toJson(sd.getAllStudent());
    }
    
    @POST
    @Path("qualifications")
    @Produces("application/json")
    public String getQualify(@PathParam("student") String student) throws Exception { 
        Student s = null;
        if (student != null) {
            s = Injection.provideGson().fromJson(student, Student.class);
            StudentDaoImp sd = new StudentDaoImp();
           return Injection.provideGson().toJson(sd.getQualifications(s));
        } else {
            return Injection.provideGson().toJson("Error");
        }
    }
    
    @POST
    @Path("enroll")
    @Produces("application/json")
    public String enrollCourse(@PathParam("student") String student, @PathParam("course") String course) throws Exception {
        Course c = null;
        Student s = null;
        if (course != null && student != null) {
            c = Injection.provideGson().fromJson(course, Course.class);
            s = Injection.provideGson().fromJson(student, Student.class);
            StudentDaoImp sd = new StudentDaoImp();
            sd.enrollCourse(s, c);
        } else {
            return Injection.provideGson().toJson("Error");
        }
        return Injection.provideGson().toJson("Registrado");
    }
    
    @POST
    @Path("deenroll")
    @Produces("application/json")
    public String deEnrollCourse(@PathParam("student") String student, @PathParam("course") String course) throws Exception {
        Course c = null;
        Student s = null;
        if (course != null && student != null) {
            c = Injection.provideGson().fromJson(course, Course.class);
            s = Injection.provideGson().fromJson(student, Student.class);
            StudentDaoImp sd = new StudentDaoImp();
            sd.enrollCourse(s, c);
        } else {
            return Injection.provideGson().toJson("Error");
        }
        return Injection.provideGson().toJson("Desmatriculado");
    }
    
    @POST
    @Path("qualify")
    @Produces("application/json")
    public String qualify(@PathParam("enrollment") String enrollment) throws Exception {
        Enrollment e = null;
        if (enrollment != null) {
            e = Injection.provideGson().fromJson(enrollment, Enrollment.class);
            StudentDaoImp sd = new StudentDaoImp();
            sd.qualify(e);
        } else {
            return Injection.provideGson().toJson("Error");
        }
        return Injection.provideGson().toJson("Desmatriculado");
    }
    
    @POST
    @Path("notenrolled")
    @Produces("application/json")
    public String getNotEnrolled(@PathParam("student") String student) throws Exception {
        Student s = null;
        if (student != null) {
            s = Injection.provideGson().fromJson(student, Student.class);
            StudentDaoImp sd = new StudentDaoImp();
           return Injection.provideGson().toJson(sd.getQualifications(s));
        } else {
            return Injection.provideGson().toJson("Error");
        }
    }

}
