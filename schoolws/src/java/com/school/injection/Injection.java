package com.school.injection;

import com.google.gson.Gson;

/**
 *
 * @author Cristian Z. Osia
 */
public class Injection {
    
    public static final Gson provideGson() {
        return new Gson();
    }
    
}
