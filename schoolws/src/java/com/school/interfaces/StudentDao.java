package com.school.interfaces;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import java.util.List;

/**
 *
 * @author Cristian Z. Osia
 */
public interface StudentDao {
    
    public String enrollCourse(Student student, Course course) throws Exception;
    public String deEnrollCourse(Student student, Course course) throws Exception;
    public List<Student> getAllStudent() throws Exception;
    public List<Course> getAllCourse() throws Exception;
    public List<Enrollment> getQualifications(Student student) throws Exception;
    public List<Enrollment> getNotEnrolled(Student student) throws Exception;
    public List<Enrollment> getAllEnrollmentByStudent(Student student) throws Exception;
    public String qualify(Enrollment enrollment) throws Exception;
    
}
