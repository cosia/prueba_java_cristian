package com.school.interactors;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.connection.ConnectionDB;
import com.school.interfaces.StudentDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cristian Z. Osia
 */
public class StudentDaoImp extends ConnectionDB implements StudentDao {

    @Override
    public String enrollCourse(Student student, Course course) throws Exception {
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("INSERT INTO \"tblMatricula\"  \"idStudent\", \"idCourse\""
                    + "    VALUES (?, ?);");
            ps.setInt(1, student.getId());
            ps.setInt(2, course.getIdCourse());
            ps.executeUpdate();
            return "Matriculado";
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public String deEnrollCourse(Student student, Course course) throws Exception {
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("DELETE FROM \"tblMatricula\" WHERE \"idStudent\" = ? AND \"idCourse\" = ?");
            ps.setInt(1, student.getId());
            ps.setInt(2, course.getIdCourse());
            ps.executeUpdate();
            return "Desmatriculado";
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public List<Enrollment> getQualifications(Student student) throws Exception {
        List<Enrollment> list;
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM \"tblMatricula\" WHERE id = ?");
            ps.setInt(1, student.getId());
            list = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Enrollment enrollment = new Enrollment();
                enrollment.setIdStudent(rs.getInt("idStudent"));
                enrollment.setIdCourse(rs.getInt("idCourse"));
                enrollment.setQualificationFromString(rs.getString("qualification"));
                enrollment.setQualified(rs.getBoolean("qualified"));
                list.add(enrollment);
            }
            return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public List<Student> getAllStudent() throws Exception {
        List<Student> list;
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM \"tblStrudent\"");
            list = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setLastName(rs.getString("lastName"));
                list.add(student);
            }
            return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public List<Course> getAllCourse() throws Exception {
        List<Course> list;
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM \"tblCourse\"");
            list = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setIdCourse(rs.getInt("id"));
                course.setNameCourse(rs.getString("nameCourse"));
                list.add(course);
            }
            return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public List<Enrollment> getAllEnrollmentByStudent(Student student) throws Exception {
        List<Enrollment> list;
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM \"tblCourse\"");
            list = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Enrollment enrollment = new Enrollment();
                enrollment.setIdStudent(rs.getInt("idStudent"));
                enrollment.setIdCourse(rs.getInt("idCourse"));
                enrollment.setQualificationFromString(rs.getString("qualification"));
                enrollment.setQualified(rs.getBoolean("qualified"));
                list.add(enrollment);
            }
            return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

    @Override
    public String qualify(Enrollment enrollment) throws Exception {
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("UPDATE \"tblMatricula\" SET qualification=?, qualified=true"
                    + " WHERE \"idStudent\"=? AND \"idCourse\"=?");
            ps.setString(1, String.valueOf(enrollment.getQualification()));
            ps.setInt(2, enrollment.getIdStudent());
            ps.setInt(3, enrollment.getIdCourse());
            ps.executeUpdate();
            return "Matriculado";
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }
    
    @Override
    public List<Enrollment> getNotEnrolled(Student student) throws Exception {
        List<Enrollment> list;
        try {
            this.connect();
            PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM \"tblMatricula\" WHERE id = ?");
            ps.setInt(1, student.getId());
            list = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Enrollment enrollment = new Enrollment();
                enrollment.setIdStudent(rs.getInt("idStudent"));
                enrollment.setIdCourse(rs.getInt("idCourse"));
                enrollment.setQualificationFromString(rs.getString("qualification"));
                enrollment.setQualified(rs.getBoolean("qualified"));
                list.add(enrollment);
            }
            return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.close();
        }
    }

}
