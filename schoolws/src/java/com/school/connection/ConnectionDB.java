package com.school.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author Cristian Z. Osia
 */
public class ConnectionDB {
    
    protected Connection connection;
    
    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_PORT = ":5432";
    private static final String DB_NAME = "/school";
    private static final String DB_URL = "jdbc:postgresql://localhost" + DB_PORT + DB_NAME;
    private static final String DB_USER = "postgres";
    private static final String DB_PASS = "pass";
    
    public void connect() throws Exception {
        try {
            Class.forName(JDBC_DRIVER);
            this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);            
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void close() throws Exception {
        if (connection != null) {
            if (!connection.isClosed()) {
                connection.close();
            }
        }
    }
    
}
