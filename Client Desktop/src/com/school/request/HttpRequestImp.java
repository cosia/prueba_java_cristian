package com.school.request;

import com.google.gson.Gson;
import com.school.classes.Student;
import com.school.interfaces.interactors.HttpRequestInteractor;
import com.school.interfaces.requestlistener.OnHttpRequestResponseRestListener;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.client.Entity;
import com.school.injection.Injection;
import java.util.ArrayList;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import com.school.classes.Course;
import com.school.classes.Enrollment;
import javax.ws.rs.client.InvocationCallback;

/**
 *
 * @author Cristian Z. Osia
 */
public class HttpRequestImp implements HttpRequestInteractor {

    private Gson gson;
    private OnHttpRequestResponseRestListener listener;
    //private Client client;

    public HttpRequestImp(Gson gson) {
        this.gson = gson;
    }

    @Override
    public void sendRequestGetAllStudents(String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    try {
                        ArrayList<Student> students;
                        Type listType = new TypeToken<ArrayList<Student>>() {
                        }.getType();
                        students = Injection.provideGson().fromJson(response, listType);
                        listener.finishGetAllStudent(students);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                }

            });
        } catch (Exception e) {
            //this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestGetAllCourses(Student student, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("student", Injection.provideGson().toJson(student));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    try {
                        ArrayList<Course> courses;
                        Type listType = new TypeToken<ArrayList<Course>>() {
                        }.getType();
                        courses = Injection.provideGson().fromJson(response, listType);
                        listener.finishGetAllCourses(courses);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                }

            });
        } catch (Exception e) {
            //this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestEnroll(Student student, Course course, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("student", Injection.provideGson().toJson(student));
            form.param("course", Injection.provideGson().toJson(course));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    try {
                        ArrayList<Course> courses;
                        Type listType = new TypeToken<ArrayList<Course>>() {
                        }.getType();
                        courses = Injection.provideGson().fromJson(response, listType);
                        listener.finishGetAllCourses(courses);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                }

            });
        } catch (Exception e) {
            //this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestDeEnroll(Student student, Course course, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("student", Injection.provideGson().toJson(student));
            form.param("course", Injection.provideGson().toJson(course));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    try {
                        ArrayList<Course> courses;
                        Type listType = new TypeToken<ArrayList<Course>>() {
                        }.getType();
                        courses = Injection.provideGson().fromJson(response, listType);
                        listener.finishGetAllCourses(courses);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                }

            });
        } catch (Exception e) {
            //this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestQualify(Enrollment enrollment, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("enrollment", Injection.provideGson().toJson(enrollment));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    try {
                        ArrayList<Course> courses;
                        Type listType = new TypeToken<ArrayList<Course>>() {
                        }.getType();
                        courses = Injection.provideGson().fromJson(response, listType);
                        listener.finishGetAllCourses(courses);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                }

            });
        } catch (Exception e) {
            //this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

}
