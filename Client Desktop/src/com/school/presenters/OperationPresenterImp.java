package com.school.presenters;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interactors.OperationInteractorImp;
import com.school.interfaces.interactors.OperationInteractor;
import com.school.interfaces.presenters.OperationPresenter;
import com.school.interfaces.requestlistener.OperationRequestResponseListener;
import com.school.interfaces.views.OperationView;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class OperationPresenterImp implements OperationPresenter, OperationRequestResponseListener {

    private OperationInteractor interactor;
    private OperationView operacionView;

    public OperationPresenterImp(OperationView operacionView) {
        initOperation(operacionView);
        this.interactor = new OperationInteractorImp();
    }
    
    @Override
    public void initOperation(OperationView operationView) {
        this.operacionView = operationView;
    }

    @Override
    public void getAllStudents() {
        if (operacionView != null) {
            interactor.getAllStudents(this);
        }
    }
    
    @Override
    public void getAllCourses(Student student) {
        if (operacionView != null) {
            interactor.getAllCourses(student, this);
        }
    }

    @Override
    public void error(String error) {
    }

    @Override
    public void setStudents(ArrayList<Student> students) {
        if (operacionView != null) {
            operacionView.setStudents(students);
        }
    }

    @Override
    public void setCourses(ArrayList<Course> courses) {
        if (operacionView != null) {
            operacionView.setCourses(courses);
        }
    }

    @Override
    public void qualify(Enrollment enrollment) {
        if (operacionView != null) {
            interactor.qualify(enrollment, this);
        }
    }

    @Override
    public void enroll(Student student, Course course) {
        if (operacionView != null) {
            interactor.enroll(student, course, this);
        }
    }

    @Override
    public void deEnroll(Student student, Course course) {
        if (operacionView != null) {
            interactor.deEnroll(student, course, this);
        }
    }

    @Override
    public void setMessage(String msg) {
        if (operacionView != null) {
            operacionView.message(msg);
        }
    }
    
}
