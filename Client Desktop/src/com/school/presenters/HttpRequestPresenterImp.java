package com.school.presenters;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interfaces.interactors.HttpRequestInteractor;
import com.school.interfaces.presenters.HttpRequestPresenter;
import com.school.interfaces.requestlistener.OnHttpRequestResponseRestListener;
import com.school.interfaces.views.HttpRequestView;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class HttpRequestPresenterImp implements HttpRequestPresenter, OnHttpRequestResponseRestListener {
    
    private HttpRequestView view;
    private HttpRequestInteractor interactor;
    private String urlServer;
    
    public HttpRequestPresenterImp(HttpRequestView view, HttpRequestInteractor httpInteractor, String urlServer) {
        init(view);
        this.interactor = httpInteractor;
        this.urlServer = urlServer;
    }

    @Override
    public void init(HttpRequestView view) {
        this.view = view;
    }

    @Override
    public void sendRequestGetAllStudent(String url) {
        interactor.sendRequestGetAllStudents(urlServer, url, this);
    }
    
    @Override
    public void sendRequestGetAllCourses(Student student, String url) {
        interactor.sendRequestGetAllCourses(student, urlServer, url, this);
    }

    @Override
    public void finishGetAllStudent(ArrayList<Student> students) {
        if (view != null) {
            view.sendGetAllStudentResponse(students);
        }
    }
    
    @Override
    public void finishGetAllCourses(ArrayList<Course> courses) {
        if (view != null) {
            view.sendGetAllCoursesResponse(courses);
        }
    }

    @Override
    public void sendRequestQualify(Enrollment enrollment, String url) {
        interactor.sendRequestQualify(enrollment, urlServer, url, this);
    }

    @Override
    public void sendRequestEnroll(Student student, Course course, String url) {
        interactor.sendRequestEnroll(student, course, urlServer, url, this);
    }

    @Override
    public void sendRequestDeEnroll(Student student, Course course, String url) {
        interactor.sendRequestDeEnroll(student, course, urlServer, url, this);
    }

    @Override
    public void finishQuality(String msg) {
        if (view != null) {
            view.sendQualityResponse(msg);
        }
    }

    @Override
    public void finishEnroll(String msg) {
        if (view != null) {
            view.sendEnrollResponse(msg);
        }
    }

    @Override
    public void finishDeEnroll(String msg) {
        if (view != null) {
            view.sendDeEnrollResponse(msg);
        }
    }
    
}
