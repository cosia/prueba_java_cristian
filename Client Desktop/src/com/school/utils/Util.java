package com.school.utils;

/**
 *
 * @author Cristian Z. Osia
 */
public class Util {
    
    public static String URL_SERVER = "http://localhost:8080/schoolws/ws";
    
    public static String TARGET_ENROLL = "student/enroll";
    public static String TARGET_ALLSTUDENT = "student/allstudents";
    public static String TARGET_ALLCOURSES = "student/qualifications";
    public static String TARGET_FINISH = "student/enroll";
    
}
