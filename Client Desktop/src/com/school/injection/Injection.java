package com.school.injection;

import com.google.gson.Gson;
import com.school.interfaces.interactors.HttpRequestInteractor;
import com.school.request.HttpRequestImp;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Cristian Z. Osia
 */
public class Injection {
    
    public static final Client provideClient(){
        return ClientBuilder.newBuilder().build();
    }
    
    public static final Gson provideGson() {
        return new Gson();
    }
    
    public static final WebTarget provideWebTarget(Client client, String url){
        return client.target(url);
    }
    
    public static final HttpRequestInteractor provideHttpRequestInteractor(){
        return new HttpRequestImp(provideGson());
    }
    
}
