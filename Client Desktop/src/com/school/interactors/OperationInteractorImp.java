package com.school.interactors;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.injection.Injection;
import com.school.interfaces.interactors.OperationInteractor;
import com.school.interfaces.presenters.HttpRequestPresenter;
import com.school.interfaces.requestlistener.OperationRequestResponseListener;
import com.school.interfaces.views.HttpRequestView;
import com.school.presenters.HttpRequestPresenterImp;
import com.school.utils.Util;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class OperationInteractorImp implements OperationInteractor, HttpRequestView {

    private HttpRequestPresenter requestPresenter;
    private OperationRequestResponseListener listener;

    public OperationInteractorImp() {
        requestPresenter = new HttpRequestPresenterImp(this, Injection.provideHttpRequestInteractor(), Util.URL_SERVER);
    }

    @Override
    public void getAllStudents(OperationRequestResponseListener listener) {
        this.listener = listener;
        requestPresenter.sendRequestGetAllStudent(Util.TARGET_ALLSTUDENT);
    }

    @Override
    public void getAllCourses(Student student, OperationRequestResponseListener listener) {
        this.listener = listener;
        requestPresenter.sendRequestGetAllCourses(student, Util.TARGET_ALLCOURSES);
    }

    @Override
    public void sendGetAllStudentResponse(ArrayList<Student> students) {
        listener.setStudents(students);
    }

    @Override
    public void sendGetAllCoursesResponse(ArrayList<Course> courses) {
        listener.setCourses(courses);
    }

    @Override
    public void qualify(Enrollment enrollment, OperationRequestResponseListener listener) {
        this.listener = listener;
        requestPresenter.sendRequestQualify(enrollment, Util.TARGET_ALLCOURSES);
    }

    @Override
    public void enroll(Student student, Course course, OperationRequestResponseListener listener) {
        this.listener = listener;
        requestPresenter.sendRequestEnroll(student, course, Util.TARGET_ALLCOURSES);
    }

    @Override
    public void deEnroll(Student student, Course course, OperationRequestResponseListener listener) {
        this.listener = listener;
        requestPresenter.sendRequestDeEnroll(student, course, Util.TARGET_ALLCOURSES);
    }

    @Override
    public void sendQualityResponse(String msg) {
        listener.setMessage(msg);
    }

    @Override
    public void sendEnrollResponse(String msg) {
        listener.setMessage(msg);
    }

    @Override
    public void sendDeEnrollResponse(String msg) {
        listener.setMessage(msg);
    }

}
