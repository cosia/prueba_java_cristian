package com.school.interfaces.presenters;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interfaces.views.HttpRequestView;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestPresenter {
    
    void init(HttpRequestView view);
    void sendRequestGetAllStudent(String url);
    void sendRequestGetAllCourses(Student student, String url);
    void sendRequestQualify(Enrollment enrollment, String url);
    void sendRequestEnroll(Student student, Course course, String url);
    void sendRequestDeEnroll(Student student, Course course, String url);
    
}
