package com.school.interfaces.presenters;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interfaces.views.OperationView;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperationPresenter {
    
    void initOperation(OperationView operationView);
    void getAllStudents();
    void getAllCourses(Student student);
    void qualify(Enrollment enrollment);
    void enroll(Student student, Course course);
    void deEnroll(Student student, Course course);
    
}
