package com.school.interfaces.interactors;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interfaces.requestlistener.OnHttpRequestResponseRestListener;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestInteractor {
    
    void sendRequestGetAllStudents(String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestGetAllCourses(Student student, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestEnroll(Student student, Course course, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestDeEnroll(Student student, Course course, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestQualify(Enrollment enrollment, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    
}
