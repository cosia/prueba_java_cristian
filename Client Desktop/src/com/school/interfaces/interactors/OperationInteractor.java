package com.school.interfaces.interactors;

import com.school.classes.Course;
import com.school.classes.Enrollment;
import com.school.classes.Student;
import com.school.interfaces.requestlistener.OperationRequestResponseListener;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperationInteractor {
    
    void getAllStudents(OperationRequestResponseListener listener);
    void getAllCourses(Student student, OperationRequestResponseListener listener);
    void qualify(Enrollment enrollment, OperationRequestResponseListener listener);
    void enroll(Student student, Course course, OperationRequestResponseListener listener);
    void deEnroll(Student student, Course course, OperationRequestResponseListener listener);
    
}
