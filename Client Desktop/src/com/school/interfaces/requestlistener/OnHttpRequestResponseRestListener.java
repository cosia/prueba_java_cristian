package com.school.interfaces.requestlistener;

import com.school.classes.Course;
import com.school.classes.Student;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OnHttpRequestResponseRestListener {
    
    void finishGetAllStudent(ArrayList<Student> students);
    void finishGetAllCourses(ArrayList<Course> courses);
    void finishQuality(String msg);
    void finishEnroll(String msg);
    void finishDeEnroll(String msg);
}
