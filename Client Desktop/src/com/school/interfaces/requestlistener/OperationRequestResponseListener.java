package com.school.interfaces.requestlistener;

import com.school.classes.Course;
import com.school.classes.Student;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperationRequestResponseListener {
    
    void error(String error);
    void setStudents(ArrayList<Student> students);
    void setCourses(ArrayList<Course> courses);
    void setMessage(String msg);
    
}
