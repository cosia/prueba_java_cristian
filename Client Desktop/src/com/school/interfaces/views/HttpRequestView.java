package com.school.interfaces.views;

import com.school.classes.Course;
import com.school.classes.Student;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestView {
    
    void sendGetAllStudentResponse(ArrayList<Student> students);
    void sendGetAllCoursesResponse(ArrayList<Course> students);
    void sendQualityResponse(String msg);
    void sendEnrollResponse(String msg);
    void sendDeEnrollResponse(String msg);
    
}
