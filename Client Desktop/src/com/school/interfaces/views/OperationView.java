package com.school.interfaces.views;

import com.school.classes.Course;
import com.school.classes.Student;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperationView {
    
    void error(String error);
    void setStudents(ArrayList<Student> students);
    void setCourses(ArrayList<Course> courses);
    void message(String msg);
    
}
