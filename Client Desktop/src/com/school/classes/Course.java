package com.school.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class Course {
    
    private int idCourse;
    private String nameCourse;

    public Course() {
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }
    
}
