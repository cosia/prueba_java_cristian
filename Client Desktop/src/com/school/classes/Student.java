package com.school.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class Student {
    
    private int id;
    private String name;
    private String lastName;

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    @Override
    public String toString() {
        return getName() + " " + getLastName(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
